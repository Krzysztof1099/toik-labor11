package com.demo.springboot.model;

public interface MovieService {
    MovieRepository getMovieRepository();
}
