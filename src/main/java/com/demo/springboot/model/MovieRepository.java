package com.demo.springboot.model;

import com.demo.springboot.dto.MovieListDto;

public interface MovieRepository {
    MovieListDto getMovieListDto();
}
