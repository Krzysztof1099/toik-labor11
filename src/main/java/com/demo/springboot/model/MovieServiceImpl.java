package com.demo.springboot.model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MovieServiceImpl implements MovieService {
    @Autowired
    private MovieRepository movieRepository;

    public MovieServiceImpl() {
        this.movieRepository = new MovieRepositoryImpl();
    }

    public MovieRepository getMovieRepository() {
        return movieRepository;
    }
}
