package com.demo.springboot.rest;


import com.demo.springboot.dto.MovieListDto;
import com.demo.springboot.model.MovieService;
import com.demo.springboot.model.MovieServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);


    @Autowired
    private MovieService movieService;

    public MovieApiController() {
        movieService=new MovieServiceImpl();
    }

    @GetMapping("/movies")
    public ResponseEntity<MovieListDto> getMovies() {
        return ResponseEntity.ok().body(movieService.getMovieRepository().getMovieListDto());
    }

}
